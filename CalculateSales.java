package jp.alhinc.onodera_ryuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<String, String>();
		Map<String, String> commodityNames = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();
		Map<String, Long> commoditySales = new HashMap<String, Long>();
		
		BufferedReader br = null;
		
		//読込メソッドの呼び出し（支店）
		if(!inPut(args[0], "branch.lst", "支店", "^[0-9]{3}$", branchNames, branchSales)) {
			return;
		}
		
		//読込メソッドの呼び出し（商品）
		if(!inPut(args[0], "commodity.lst", "商品", "^[0-9a-zA-Z]{8}$", commodityNames, commoditySales)) {
			return;
		}

		//listFiles()で指定したパス内のすべてのファイルの情報が格納される
		File[] files = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<File>();

		//指定パス内のファイル名をすべて表示する
		for(int i = 0; i < files.length; i++) {

			//file型からString型に変換し.getName()が使用できるように
			String fileName = files[i].getName();

			/*ファイルとディレクトリどちらなのか、
			数字8桁＋ファイル名の末がrcdのファイル名かどうかを判定する。
			trueならrcdFilesにaddする*/
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルが連番になっているかどうか確認する
		for(int i = 0; i < rcdFiles.size() -1; i++) {

			//formerとlatterに1の差があるかどうかで確認
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
			
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				ArrayList<String> salesData = new ArrayList<String>();

				//リストに値を保持させる
				while((line = br.readLine()) != null) {
					salesData.add(line);
				}
				
				if(salesData.size() != 3) {
					System.out.println(salesData.get(i) + "のフォーマットが不正です");
					return;
				}
				
				//支店コードが合っているか判定
				if(!branchNames.containsKey(salesData.get(0))) {
					System.out.println(salesData.get(0) + "の支店コードが不正です");
					return;
				}
				
				if(!commodityNames.containsKey(salesData.get(1))) {
					System.out.println(salesData.get(1) + "の商品コードが不正です");
					return;
				}
					
				//売上金額が数値になっているかを判定
				if(!salesData.get(2).matches("[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");	
					return;
				}
				
				/*売上金額を型変換をする（for文内で回しているので、
				 * get(2)ですべてのファイルから金額を読み込める
				 * saleAmountに支店ごとの売上金額
				 * commodityAmountsに商品ごとの売り上げ*/
				Long fileSale = Long.parseLong(salesData.get(2));
				Long saleAmount = branchSales.get(salesData.get(0)) + fileSale;
				Long commodityAmounts = commoditySales.get(salesData.get(1)) + fileSale;

				//支店ごと合計金額が11桁以上になっていないか判定
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				
				//商品ごと集計の合計金額が11桁以上になっていないか判定
				if(commodityAmounts >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//branchSalesに支店コードと支店合計金額をput
				branchSales.put(salesData.get(0), saleAmount);
				//commoditySalesに商品コードと商品合計金額put
				commoditySales.put(salesData.get(1), commodityAmounts);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		
		//出力メソッドの呼び出し（支店）
		if(!outPut(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		
		//出力メソッドの呼び出し（商品）
		if(!outPut(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	private static boolean inPut(String path, String fileName, String definition, String regex, Map<String, String> names, Map<String, Long> sales) {
		
		BufferedReader br = null;
		
		try {
			//FikeReaderでbrabch.lstを読み込む
			/*FileReaderで1文字ずつ読み込み。BufferedReaderで1行ずつ読み込む
			 （BufferedReaderはFileReaderの拡張機能のため、ステップを踏む必要あり)
			 */

			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(definition + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

				//定義ファイルの読み込みが完了

				//split(",")でカンマ区切りのデータを分割
				String[] items = line.split(",");
				if((items.length != 2) && (!items[0].matches(regex))) {
					System.out.println(definition + "定義ファイルのフォーマットが不正です");
					return false;
				}

				//読み込んだデータをHashMapで保持する
				//HashMapにputで情報を追加
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
			
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
			
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	
	private static boolean outPut(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		
		BufferedWriter bw = null;
		
		try {

			//書き込むファイルを指定する
			File totalFile = new File(path, fileName);
			FileWriter fw = new FileWriter(totalFile);
			bw = new BufferedWriter(fw);

			//MapからすべてのKeyを取得する
			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
